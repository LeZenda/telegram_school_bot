import random

MOODS = [
    "I’m good.",
    "Pretty good.",
    "Just the same old same old",
    "Oh gosh, all kinds of stuff!",
    "I’m better than I was, but not nearly as good as I’m going to be.",
    "Not too bad.",
]


def catch_where(bot, chat):
    bot.send_message(chat, "In Lviv")


def mood_response(bot, chat):
    bot.send_message(chat, random.choice(MOODS))


def ger_random_number(bot, chat):
    bot.send_message(chat, str(random.randint(1, 12)))
