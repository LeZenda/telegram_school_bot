import telebot
import config

from buttons import *
from dbhelper import DBHelper

from telebot import types

bot = telebot.TeleBot(config.TOKEN, parse_mode='html')
db = DBHelper()


@bot.message_handler(commands=['start'])
def start_command(message):
    bot.send_message(message.chat.id, f"Hello my dear friend {message.from_user.first_name} "
                                      f"{message.from_user.id if message.from_user.id else ''}", parse_mode='html')


@bot.message_handler(commands=['help'])
def help_command(message):
    # keyboard
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Random number")
    item2 = types.KeyboardButton("How are you?")
    item3 = types.KeyboardButton("Where?")

    markup.add(item1, item2, item3)
    bot.send_message(message.chat.id, f"How can I help you {message.from_user.first_name} ?", reply_markup=markup)


@bot.message_handler(commands=['list'])
def list_command(message):
    items = db.get_items()
    text = ""
    for item in items:
        text = text + str(item) + '\n'
    bot.send_message(message.chat.id, text)


@bot.message_handler(commands=['my_list'])
def my_list_command(message):

    chat = message.chat.id
    items = db.filter_items(chat)

    text = ""
    for item in items:
        text = text + str(item) + '\n'

    bot.send_message(message.chat.id, text)


@bot.message_handler(content_types=['text'])
def text_message(message):

    text = message.text
    chat = message.chat.id
    db.add_item(text, chat)

    if text == "Random number":
        ger_random_number(bot, chat)
    elif text == "How are you?":
        mood_response(bot, chat)
    elif text == "Where am I?":
        catch_where(bot, chat)
    else:
        bot.send_message(chat, "Sorry, I don't understand you")


def main():
    # ініціалізація (створення) бази данних
    db.setup()

    # Запуск бота
    bot.polling(none_stop=True)


if __name__ == '__main__':
    # При імпорті кожен файл отримує змінну '__name__' котра дорівнює імені самого файлу
    # (тобто при імпорті config.py всередині нього буде змінна __name__ == 'config')
    # в файлі котрий запускається (являєтьсяо сновним, куди все потім і імпортується)
    # отримує __name__ котрий дорівнює '__main__' таким чином, можна добавити додатковий функціонал до модуля
    # якщо його запускають а не імпортують, а в разі імпорту лише надавати доступ до функцій чи класів

    main()
